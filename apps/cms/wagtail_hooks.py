from django.conf import settings
from django.urls import reverse
from django.utils.html import format_html_join
from wagtail import hooks
from wagtail.admin.menu import MenuItem


@hooks.register("insert_global_admin_css")
def global_css():
    css_files = ["css/font-awesome.min.css", "cms/css/wagtail-font-awesome.css"]

    css_includes = format_html_join(
        "\n",
        '<link rel="stylesheet" href="{0}{1}">',
        ((settings.STATIC_URL, filename) for filename in css_files),
    )

    return css_includes


@hooks.register("insert_editor_css")
def editor_css():
    css_files = ["css/font-awesome.min.css", "cms/css/wagtail-font-awesome.css"]

    css_includes = format_html_join(
        "\n",
        '<link rel="stylesheet" href="{0}{1}">',
        ((settings.STATIC_URL, filename) for filename in css_files),
    )

    return css_includes


class DjangoAdminLinkItem:
    @staticmethod
    def render(request):
        return """<div class="wagtail-userbar__item ">
                    <div class="wagtail-action wagtail-icon wagtail-icon-pick">
                        <a href="/{}" target="_parent">Admin</a>
                    </div>
                </div>""".format(
            settings.ADMIN_URL
        )


@hooks.register("construct_wagtail_userbar")
def add_wagtail_icon_items(request, items):
    if request.user.is_superuser:
        items.insert(0, DjangoAdminLinkItem())


@hooks.register("register_icons")
def register_icons(icons):
    return icons + ["cms/circle-stack.svg"]


@hooks.register("construct_main_menu")
def main_menu_django_admin_item(request, menu_items):
    if request.user.is_superuser:
        menu_items.append(
            MenuItem(
                label="Admin",
                icon_name="circle-stack",
                url=reverse("admin:index"),
                order=90000,
            )
        )
