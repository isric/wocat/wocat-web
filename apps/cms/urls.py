from django.urls import re_path

from .views import DocumentUploadView, DocumentUploadDeleteView

urlpatterns = [
    re_path(
        r"^upload/(?P<page_pk>[\d]+)/(?P<module_id>[\d]+)/$",
        view=DocumentUploadView.as_view(),
        name="cms_upload",
    ),
    re_path(
        r"^upload-delete/(?P<document_id>[\d]+)/$",
        view=DocumentUploadDeleteView.as_view(),
        name="cms_upload-delete",
    ),
]
