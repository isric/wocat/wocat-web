from django.contrib.auth.models import Group
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from wagtail import hooks
from wagtail.admin.menu import MenuItem


@hooks.register("construct_main_menu")
def main_menu_django_admin_item(request, menu_items):
    """
    Adding a newsletter management item to the cms menu.
    Checking permission to group "Newsletter Moderators".
    """
    user = request.user
    newsletter_group, created = Group.objects.get_or_create(
        name="Newsletter Moderators"
    )
    if user and newsletter_group in user.groups.all():
        menu_items.append(
            MenuItem(label=_("Newsletter"), url=reverse("newsletter_index"), order=8000)
        )
