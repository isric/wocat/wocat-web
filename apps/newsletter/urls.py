from django.urls import path

from .views import NewsletterManagementView, NewsletterUnsubscribeView

urlpatterns = [
    path(
        "management/", view=NewsletterManagementView.as_view(), name="newsletter_index"
    ),
    path(
        "unsubscribe/",
        view=NewsletterUnsubscribeView.as_view(),
        name="newsletter_unsubscribe",
    ),
]
