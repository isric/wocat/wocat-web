from django.apps import AppConfig


class NewsletterConfig(AppConfig):
    name = "apps.newsletter"

    def ready(self):
        from . import signals  # noqa
