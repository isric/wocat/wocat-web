from django.apps import AppConfig


class PiwikConfig(AppConfig):
    name = "apps.piwik"
