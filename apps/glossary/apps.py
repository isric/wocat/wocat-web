from django.apps import AppConfig


class GlossaryConfig(AppConfig):
    name = "apps.glossary"
