from django.urls import path

from .views import EntryDetailView, EntryListView

urlpatterns = [
    path("list/", view=EntryListView.as_view(), name="glossary_list"),
    path("<slug:slug>/", view=EntryDetailView.as_view(), name="glossary_detail"),
]
