from django.utils.translation import gettext_lazy as _
from wagtail import hooks
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import Entry


@hooks.register("register_icons")
def register_icons(icons):
    return icons + ["glossary/book-open.svg"]


@modeladmin_register
class EntryModelAdmin(ModelAdmin):
    model = Entry
    menu_label = _("Glossary")
    menu_icon = "book-open"
    list_display = ["title"]
    list_filter = ["acronym"]
    search_fields = ["title", "description"]
