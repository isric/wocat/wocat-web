from autoslug import AutoSlugField
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from wagtail.admin.panels import FieldPanel
from wagtail.fields import StreamField
from wagtail.models import Page
from wagtail.search import index

from apps.cms.models import HeaderPageMixin, UniquePageMixin
from apps.cms.translation import TranslatablePageMixin
from apps.core.blocks import CORE_BLOCKS


class Entry(index.Indexed, models.Model):
    title = models.CharField("Title", max_length=255, unique=True)
    slug = AutoSlugField(populate_from="title")
    description = models.TextField(
        "Description",
    )
    acronym = models.BooleanField(
        "Is an acronym",
        default=False,
    )

    class Meta:
        verbose_name = "Entry"
        verbose_name_plural = "Entries"
        ordering = ["title"]

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("glossary_detail", args=[self.slug])

    panels = [
        FieldPanel("title"),
        # FieldPanel('slug'),
        FieldPanel("description"),
        FieldPanel("acronym"),
    ]

    search_fields = [
        index.SearchField("title"),
        index.SearchField("description"),
        index.FilterField("acronym"),
    ]


class GlossaryPage(UniquePageMixin, HeaderPageMixin, TranslatablePageMixin, Page):
    template = "pages/glossary.html"

    content = StreamField(CORE_BLOCKS, blank=True, use_json_field=True)

    content_panels = (
        Page.content_panels
        + HeaderPageMixin.content_panels
        + [FieldPanel("content")]
        + TranslatablePageMixin.content_panels
    )

    search_fields = (
        Page.search_fields
        + HeaderPageMixin.search_fields
        + [index.SearchField("content")]
    )

    class Meta:
        verbose_name = _("Glossary")

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)
        context["entries"] = Entry.objects.all()
        return context
