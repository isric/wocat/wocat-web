from django.urls import path

from . import views

urlpatterns = [
    # URL pattern for the UserListView
    path("", view=views.UserListView.as_view(), name="list"),
    # URL pattern for the UserRedirectView
    path("~redirect/", view=views.UserRedirectView.as_view(), name="redirect"),
    # URL pattern for the UserUpdateView
    path("~update/", view=views.UserUpdateView.as_view(), name="update"),
    # URL pattern for the UserDetailView
    path(r"<int:pk>", view=views.UserDetailView.as_view(), name="detail"),
    path(
        "mailchimp-update/",
        view=views.MailChimpUpdateView.as_view(),
        name="mailchimp-update",
    ),
]
