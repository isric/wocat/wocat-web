from django.utils.translation import gettext_lazy as _
from generic_chooser.views import ModelChooserViewSet
from wagtail import hooks

from .models import User


class UserChooserViewSet(ModelChooserViewSet):
    icon = "user"
    model = User
    page_title = _("Choose a user")
    per_page = 100
    order_by = "first_name"
    # fields = ["first_name", "last_name", "email"]


@hooks.register("register_admin_viewset")
def register_user_chooser_viewset():
    return UserChooserViewSet("user_chooser", url_prefix="user-chooser")
