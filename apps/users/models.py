from allauth.account.signals import user_signed_up, email_confirmed
from allauth.account.utils import send_email_confirmation
from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin, Group
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.db import models
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from easy_thumbnails.fields import ThumbnailerImageField
from wagtail.snippets.models import register_snippet

from apps.countries.models import Country


@register_snippet
class UserExperience(models.Model):
    name = models.CharField(_("Experience"), unique=True)

    def __str__(self):
        return self.name


@register_snippet
class UserKeyWorkTopic(models.Model):
    name = models.CharField(_("Key work topics"), unique=True)

    def __str__(self):
        return self.name


class UserManager(BaseUserManager):
    def create_user(self, email, first_name, last_name, password=None):
        if not email:
            raise ValueError(_("Users must have an email address"))

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email, password=password, first_name=first_name, last_name=last_name
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]

    email = models.EmailField(verbose_name=_("Email address"), unique=True)
    first_name = models.CharField(verbose_name=_("First name"), blank=True)
    last_name = models.CharField(verbose_name=_("Last name"), blank=True)
    is_staff = models.BooleanField(
        "staff status",
        default=False,
        help_text="Designates whether the user can log into this admin site.",
    )
    is_active = models.BooleanField(
        "active",
        default=True,
        help_text="Designates whether this user should be treated as active. "
        "Unselect this instead of deleting accounts.",
    )

    is_member = models.BooleanField(_("is WOCAT Member"), default=False)

    date_joined = models.DateTimeField("date joined", default=timezone.now)

    MALE = "m"
    FEMALE = "f"
    GENDER_CHOICES = ((MALE, (_("Mr"))), (FEMALE, (_("Mrs"))))

    gender = models.CharField(
        verbose_name=_("Salutation"), max_length=1, choices=GENDER_CHOICES, blank=True
    )
    title = models.CharField(verbose_name=_("Title"), blank=True)
    position = models.CharField(verbose_name=_("Position"), blank=True)
    department = models.CharField(verbose_name=_("Department"), blank=True)
    LEVEL_1 = 1
    LEVEL_2 = 2
    LEVEL_3 = 3
    FUNCTION_CHOICES = (
        (LEVEL_1, _("SLM specialists field level")),
        (LEVEL_2, _("SLM specialists (sub-)national level")),
        (LEVEL_3, _("SLM specialists regional and global levels")),
    )
    function = models.PositiveSmallIntegerField(
        verbose_name=_("Your function"), choices=FUNCTION_CHOICES, blank=True, null=True
    )

    experiences = models.ManyToManyField(UserExperience, blank=True)
    key_work_topics = models.ManyToManyField(UserKeyWorkTopic, blank=True)

    address = models.CharField(_("Address Information"), blank=True)
    address_2 = models.CharField(_("Address 2"), blank=True)
    postal_code = models.CharField(_("Postal code"), blank=True)
    city = models.CharField(_("City"), blank=True)
    country = models.ForeignKey(
        Country, blank=True, null=True, on_delete=models.PROTECT
    )

    phone = models.CharField(_("Phone"), blank=True)
    phone_2 = models.CharField(_("Phone 2"), blank=True)
    fax = models.CharField(_("Fax"), blank=True)
    fax_2 = models.CharField(_("Fax 2"), blank=True)
    second_email = models.EmailField(_("Second email"), blank=True)
    language = models.CharField(
        verbose_name=_("Language"),
        max_length=2,
        choices=settings.LANGUAGES,
        default=settings.LANGUAGES[0][0],
    )
    comments = models.TextField(verbose_name=_("Comments"), blank=True)
    avatar = ThumbnailerImageField(
        verbose_name="avatar",
        upload_to="users/avatars",
        # resize_source=dict(size=(1200, 1200)),
        blank=True,
    )
    institution = models.ForeignKey(
        "institutions.Institution", on_delete=models.SET_NULL, blank=True, null=True
    )
    newsletter = models.BooleanField(_("Newsletter subscription"), default=False)
    terms_and_conditions = models.BooleanField(
        _("Accepted terms and conditions"), default=False
    )
    deprecated = models.TextField(verbose_name="Deprecated data", blank=True)

    objects = UserManager()

    def __str__(self):
        return self.name

    @property
    def experiences_display(self):
        return ", ".join(str(experience) for experience in self.experiences.all())

    @property
    def key_work_topics_display(self):
        return ", ".join(str(topic) for topic in self.key_work_topics.all())

    @property
    def full_address(self):
        address = self.address
        if self.postal_code:
            address += f", {self.postal_code}"
        if self.city:
            address += f" {self.city}"
        return address

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"pk": self.id})

    @property
    def detail_url(self):
        return self.get_absolute_url()

    @property
    def update_url(self):
        return reverse("users:update")

    @property
    def name(self):
        return self.get_full_name()

    def get_full_name(self):
        return f"{self.first_name} {self.last_name}"

    def get_short_name(self):
        return self.first_name

    @property
    def email_safe(self):
        email = self.email
        return email.replace("@", "( at )")

    def unsubscribe(self):
        self.newsletter = False
        self.save()

    class Meta(AbstractBaseUser.Meta):
        ordering = ("first_name", "last_name")
        verbose_name = _("User")
        verbose_name_plural = _("Users")


# Step 1: Signed up
@receiver(user_signed_up)
def send_email_confirmation_on_signup(request, user, **kwargs):
    send_email_confirmation(request, user, signup=False)


# Step 2: Email confirmed
@receiver(email_confirmed)
def deactivate_user_on_email_confirmation(email_address, **kwargs):
    user = email_address.user
    user.is_active = False
    user.save()
    notify_moderators(user)


# Commented out 2021 August
#  - this allowed circumvention of approval from admins for a new account
# # Reactivate users after resetting password.
# @receiver(signal=password_reset)
# def reactivate_account(user, **kwargs):
#     user.is_active = True
#     user.save()


def notify_moderators(user):
    context = {
        "user": user,
        "project": "WOCAT",
        "management_url": "https://{domain}/cms/users/{id}/".format(
            domain=Site.objects.get_current(), id=user.id
        ),
    }
    subject = render_to_string(
        "users/emails/email_signup_moderation_request_subject.txt", context=context
    ).strip()
    message = render_to_string(
        "users/emails/email_signup_moderation_request_message.txt", context=context
    )
    recipient_list = []
    group, created = Group.objects.get_or_create(name="Signup Moderators")
    if group:
        recipient_list += list(group.user_set.values_list("email", flat=True))
    send_mail(
        subject=subject,
        message=message,
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=recipient_list,
    )
