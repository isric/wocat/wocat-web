from django.core.management.commands import makemessages


class Command(makemessages.Command):
    msgmerge_options = [
        "-q",
        "--backup=none",
        "--previous",
        "--update",
        "--no-fuzzy-matching",
        "--no-wrap",
        # "--no-location",
    ]
