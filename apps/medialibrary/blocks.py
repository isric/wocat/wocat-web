from django.utils.functional import cached_property
from django.utils.translation import gettext_lazy as _
from generic_chooser.widgets import AdminChooser
from wagtail.blocks import StructBlock, ChoiceBlock, ChooserBlock


class MediaChooserBlock(ChooserBlock):
    @cached_property
    def target_model(self):
        from .models import Media

        return Media

    @cached_property
    def widget(self):
        from .models import Media

        class MediaChooser(AdminChooser):
            choose_one_text = _("Choose media")
            choose_another_text = _("Choose other media")
            model = Media
            choose_modal_url_name = "media_chooser:choose"
            icon = "media"

        return MediaChooser()


class MediaTeaserBlock(StructBlock):
    media = MediaChooserBlock()
    image_position = ChoiceBlock(
        choices=[("top", "Top"), ("left", "Left"), ("right", "Right")],
        required=False,
    )

    def get_context(self, value, parent_context=None):
        media = value.get("media")
        if not media:
            # Media can be deleted, there is no cascading to the teaser-object in case this happens.
            return {}

        teaser_image = (
            media.teaser_image.get_rendition("max-1200x1200").url
            if media.teaser_image
            else ""
        )
        author = media.author
        year = media.year
        languages = [language.name for language in media.languages.all()]
        image_position = value.get("image_position")

        if not media.content and media.file:
            href = media.file.url
            readmorelink = _("Download")
        else:
            href = media.get_absolute_url()
            readmorelink = _("Show media")
        return {
            "href": href,
            "title": media.title,
            "description": media.abstract,
            "author": "{author}{year}{languages}".format(
                author=f"Author: {author}" if author else "",
                year="{}Year: {}".format(", " if author else "", year) if year else "",
                languages="{}Languages: {}".format(
                    ", " if author or year else "", ", ".join(languages)
                )
                if languages
                else "",
            ),
            "readmorelink": {"text": readmorelink},
            "imgsrc": teaser_image,
            "imgpos": image_position or "top",
            "mediastyle": True,
        }

    class Meta:
        icon = "fa fa-file"
        label = "Media Teaser"
        template = "widgets/teaser.html"
