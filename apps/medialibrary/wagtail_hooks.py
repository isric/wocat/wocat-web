from django.utils.translation import gettext_lazy as _
from generic_chooser.views import ModelChooserViewSet
from wagtail import hooks
from wagtail.contrib.modeladmin.options import ModelAdmin, modeladmin_register

from .models import Media


@hooks.register("register_icons")
def register_icons(icons):
    return icons + ["wagtailadmin/icons/media.svg"]


@modeladmin_register
class MediaModelAdmin(ModelAdmin):
    model = Media
    menu_label = _("Media Library")
    menu_icon = "media"
    list_display = ["title", "author", "continent"]
    list_filter = ["author", "continent", "countries"]
    search_fields = ["title"]


class MediaChooserViewSet(ModelChooserViewSet):
    icon = "media"
    model = Media
    page_title = _("Choose media")
    per_page = 100
    order_by = "title"


@hooks.register("register_admin_viewset")
def register_media_chooser_viewset():
    return MediaChooserViewSet("media_chooser", url_prefix="media-chooser")
