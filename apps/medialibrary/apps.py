from django.apps import AppConfig


class MedialibraryConfig(AppConfig):
    name = "apps.medialibrary"
