from django.views.generic import DetailView

from .models import Media


class MediaDetailView(DetailView):
    model = Media
    template_name = "medialibrary/media.html"
