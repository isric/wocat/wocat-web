from django.urls import path, re_path

from .views import StyleguideView

urlpatterns = [
    path("", StyleguideView.as_view(), name="styleguide"),
    re_path(r"^demos/(?P<template>.*)/$", StyleguideView.as_view(), name="styleguide"),
]
