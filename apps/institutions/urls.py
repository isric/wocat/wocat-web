from django.urls import path

from .views import InstitutionDetailView, InstitutionListView

urlpatterns = [
    path("", view=InstitutionListView.as_view(), name="institutions_list"),
    path(
        "<slug:slug>/", view=InstitutionDetailView.as_view(), name="institutions_detail"
    ),
]
