from django.urls import path

from .views import CountryDetailView, CountryListView

urlpatterns = [
    path("", view=CountryListView.as_view(), name="country_list"),
    path("<slug:slug>/", view=CountryDetailView.as_view(), name="country_detail"),
]
