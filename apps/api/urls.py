from django.urls import path, re_path, include
from drf_spectacular.views import (
    SpectacularRedocView,
    SpectacularAPIView,
    SpectacularSwaggerView,
)

from apps.api.views import router
from . import views

v1_urlpatterns = [
    path("", include(router.urls)),
    path(
        "country-detail/", views.CountryCodeDetailView.as_view(), name="country-detail"
    ),
    re_path(
        r"^map/search/(?P<filter>\w+)/$",
        views.MapSearchView.as_view(),
        name="map_search",
    ),
    path("auth/login/", views.LoginView().as_view()),
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path("docs/", SpectacularSwaggerView.as_view(url_name="schema")),
    path("redoc/", SpectacularRedocView.as_view(url_name="schema")),
]

urlpatterns = [path("v1/", include(v1_urlpatterns))]
