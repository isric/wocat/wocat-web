from .base import *  # noqa

DEBUG = env.bool("DJANGO_DEBUG", default=True)
SECRET_KEY = env("DJANGO_SECRET_KEY", default="CHANGEME")
ALLOWED_HOSTS = ["*"]
INTERNAL_IPS = ("127.0.0.1", "10.0.2.2")

try:
    import django_extensions

    INSTALLED_APPS += ["django_extensions"]
except ModuleNotFoundError:
    print("not using django_extensions...")
    pass

RECAPTCHA_PUBLIC_KEY = "6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI"
RECAPTCHA_PRIVATE_KEY = "6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe"
SILENCED_SYSTEM_CHECKS = ["captcha.recaptcha_test_key_error"]
