import environ
from django.utils.translation import gettext_lazy as _


ENV_PATH = "./.env"
BASE_DIR: environ.Path = environ.Path(__file__) - 3
env = environ.Env()
env.read_env(BASE_DIR(ENV_PATH))
# ROOT_DIR = environ.Path(__file__) - 3  # (/a/b/myfile.py - 3 = /)
APPS_DIR = BASE_DIR.path("apps")

WAGTAIL_SITE_NAME = "WOCAT"
WAGTAILADMIN_BASE_URL = "https://wocat.net"

# APP CONFIGURATION
# ------------------------------------------------------------------------------
INSTALLED_APPS = [
    "apps.users",
    "apps.core",
    "apps.cms",
    "apps.styleguide",
    "apps.institutions",
    "apps.medialibrary",
    "apps.news",
    "apps.glossary",
    "apps.search",
    "apps.api",
    "apps.countries",
    "apps.languages",
    "apps.newsletter",
    "apps.piwik",
    # wagtail
    "wagtail.admin",
    "wagtail",
    "wagtail.documents",
    "wagtail.embeds",
    "wagtail.images",
    "wagtail.search",
    "wagtail.sites",
    "wagtail.snippets",
    "wagtail.users",
    "wagtail.contrib.forms",
    "wagtail.contrib.modeladmin",
    "wagtail.contrib.redirects",
    "wagtail.contrib.settings",
    "taggit",
    "modelcluster",
    # django
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sitemaps",
    "django.contrib.admin",
    # third party
    "crispy_forms",  # Form layouts
    "allauth",  # registration
    "allauth.account",  # registration
    "allauth.socialaccount",  # registration
    "cookielaw",
    "django_countries",
    "easy_thumbnails",
    "rest_framework",  # api
    "rest_framework.authtoken",  # token auth
    "drf_spectacular",
    "django_filters",
    "captcha",
    "generic_chooser",
]

LANGUAGE_CODE = "en"
LANGUAGES = [("en", _("English"))]
TIME_ZONE = "Europe/Berlin"
USE_I18N = True
USE_TZ = True
SITE_ID = 1
SITE_PROTOCOL = "https"

# https://django-environ.readthedocs.io/en/latest/types.html?highlight=smtp#environ-env-email-url
EMAIL_CONFIG = env.email_url("DJANGO_EMAIL_URL", default="consolemail://")
vars().update(EMAIL_CONFIG)
# SERVER_EMAIL = EMAIL_CONFIG["EMAIL_HOST_USER"]  # TODO check this
DEFAULT_FROM_EMAIL = env(
    "DJANGO_DEFAULT_FROM_EMAIL", default="Wocat NOREPLY <wocat-noreply@wocat.net>"
)
EMAIL_SUBJECT_PREFIX = env("DJANGO_EMAIL_SUBJECT_PREFIX", default="[WOCAT] ")

ALLOWED_HOSTS = env.list("DJANGO_ALLOWED_HOSTS", default=["wocat.net"])

ROOT_URLCONF = "config.urls"
WSGI_APPLICATION = "config.wsgi.application"

DATABASES = {"default": env.db("DATABASE_URL")}
DATABASES["default"]["ATOMIC_REQUESTS"] = True
DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

MIGRATION_MODULES = {"sites": "apps.contrib.sites.migrations"}
FIXTURE_DIRS = [APPS_DIR("fixtures")]

LOCALE_PATHS = [BASE_DIR("config/locale")]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "whitenoise.middleware.WhiteNoiseMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    # wagtail cms
    # 'wagtail.middleware.SiteMiddleware',
    "wagtail.contrib.legacy.sitemiddleware.SiteMiddleware",
    "wagtail.contrib.redirects.middleware.RedirectMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "DIRS": [str(APPS_DIR.path("templates"))],
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "apps.core.context_processors.template_settings",
            ],
        },
    },
]

CACHES = {"default": env.cache_url("DJANGO_CACHE_URL", default="dummycache://")}


CRISPY_TEMPLATE_PACK = "bootstrap3"
STATIC_ROOT = BASE_DIR("static-collected")
STATIC_URL = "/static/"
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)
STATICFILES_DIRS = [
    BASE_DIR("static"),
    BASE_DIR("node_modules/jquery/dist"),
    BASE_DIR("node_modules/bootstrap/dist"),
    BASE_DIR("node_modules/leaflet/dist"),
    BASE_DIR("node_modules/dropzone/dist/min"),
    BASE_DIR("node_modules/interactjs/dist"),
    BASE_DIR("node_modules/font-awesome"),
    BASE_DIR("node_modules/select2/dist"),
    BASE_DIR("node_modules/bootstrap-table/dist"),
]
STATICFILES_STORAGE = "whitenoise.storage.CompressedStaticFilesStorage"

MEDIA_ROOT = BASE_DIR("media")
MEDIA_URL = "/media/"

AUTHENTICATION_BACKENDS = (
    "django.contrib.auth.backends.ModelBackend",
    "allauth.account.auth_backends.AuthenticationBackend",
)

# Some really nice defaults
ACCOUNT_AUTHENTICATION_METHOD = "email"
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = "mandatory"
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_USER_MODEL_USERNAME_FIELD = None

ACCOUNT_ALLOW_REGISTRATION = env.bool("DJANGO_ACCOUNT_ALLOW_REGISTRATION", True)
ACCOUNT_ADAPTER = "apps.users.adapters.AccountAdapter"
SOCIALACCOUNT_ADAPTER = "apps.users.adapters.SocialAccountAdapter"
ACCOUNT_SIGNUP_FORM_CLASS = "apps.users.forms.UserForm"
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = "/accounts/inactive/"

RECAPTCHA_USE_SSL = env.bool("GOOGLE_RECAPTCHA_SSL", default=True)
RECPATCHA_REQUIRED_SCORE = env("GOOGLE_RECAPTCHA_V3_SCORE", default="0.90")

# Custom user app defaults
# Select the correct user model
AUTH_USER_MODEL = "users.User"
LOGIN_REDIRECT_URL = "users:redirect"
LOGIN_URL = "account_login"

DATA_UPLOAD_MAX_NUMBER_FIELDS = env.int("DATA_UPLOAD_MAX_NUMBER_FIELDS", default=1000)

# SLUGLIFIER
AUTOSLUG_SLUGIFY_FUNCTION = "slugify.slugify"

ADMIN_URL = env("DJANGO_ADMIN_URL", default="admin")

WAGTAILADMIN_NOTIFICATION_USE_HTML = True
TAGGIT_CASE_INSENSITIVE = True
WAGTAIL_USER_EDIT_FORM = "apps.cms.forms.UserEditForm"
WAGTAIL_USER_CREATION_FORM = "apps.cms.forms.UserCreationForm"
WAGTAIL_USER_CUSTOM_FIELDS = ["institution"]

WAGTAILSEARCH_BACKENDS = {
    "default": {
        "BACKEND": "wagtail.search.backends.{}".format(
            env("WAGTAILSEARCH_BACKEND", default="database")
        ),
        "TIMEOUT": 30,
        # "URLS": [ELASTICSEARCH_URL],
        # "INDEX": "wocat",
    }
}

# WAGTAILDOCS_DOCUMENT_MODEL = 'core.IndexedDocument'
# WAGTAIL_APPEND_SLASH = False


# EASY THUMBNAILS
# ------------------------------------------------------------------------------
THUMBNAIL_ALIASES = {
    "": {
        "small": {"size": (50, 50), "crop": True},
        "medium": {"size": (100, 100), "crop": True},
        "large": {"size": (200, 200), "crop": True},
        "avatarsquare": {"size": (300, 300), "crop": "smart", "upscale": True},
    },
}
THUMBNAIL_HIGH_RESOLUTION = True
THUMBNAIL_PROCESSORS = (
    "easy_thumbnails.processors.colorspace",
    "easy_thumbnails.processors.autocrop",
    "easy_thumbnails.processors.scale_and_crop",
    "easy_thumbnails.processors.filters",
)
THUMBNAIL_BASEDIR = "thumbnails"


REST_FRAMEWORK = {
    "DEFAULT_PERMISSION_CLASSES": ["rest_framework.permissions.IsAuthenticated"],
    "DEFAULT_THROTTLE_RATES": {"user": "60/m"},
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
}
SPECTACULAR_SETTINGS = {
    "TITLE": "WOCAT REST API",
    "DESCRIPTION": "WOCAT is a global network on Sustainable Land Management (SLM) that promotes the documentation, sharing and use of knowledge to support adaptation, innovation and decision-making in SLM.",
    "VERSION": "1.0.0",
    "SERVE_INCLUDE_SCHEMA": False,
    # OTHER SETTINGS
}

# NEWSLETTER
# ------------------------------------------------------------------------------
NEWSLETTER_IS_ACTIVE_SYNC = env.bool("NEWSLETTER_IS_ACTIVE_SYNC", default=False)
NEWSLETTER_USERNAME = env("NEWSLETTER_USERNAME", default="")
NEWSLETTER_API_KEY = env("NEWSLETTER_API_KEY", default="")
NEWSLETTER_LIST_ID = env("NEWSLETTER_LIST_ID", default="")
FILTERS_DISABLE_HELP_TEXT = True

# Google webmaster tools
GOOGLE_WEBMASTER_TOOLS_KEY = env("GOOGLE_WEBMASTER_TOOLS_KEY", default="")
PIWIK_SITE_ID = env("PIWIK_SITE_ID", default=None)

# Filename of js-file with geojson for all countries; used for the map. Relative
# to wocat/static/js
MAP_GEOJSON_FILE = env("MAP_GEOJSON_FILE", default="countries-20170619.geo.json")

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {"format": "%(levelname)s %(asctime)s %(module)s %(message)s"},
    },
    "handlers": {
        "file": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": "logs/django.log",
            "formatter": "verbose",
        },
        "mailchimp": {
            "level": "DEBUG",
            "class": "logging.FileHandler",
            "filename": "logs/mailchimp.log",
            "formatter": "verbose",
        },
    },
    "loggers": {
        "": {"handlers": ["file"], "propagate": True, "level": "WARNING"},
        "mailchimp_client": {
            "handlers": ["mailchimp"],
            "propagate": True,
            "level": "INFO",
        },
    },
}

# Feature toggles
FEATURE_SHOW_TRANSLATIONS = env.bool("FEATURE_SHOW_TRANSLATIONS", default=False)
