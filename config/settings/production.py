import random
import string

import sentry_sdk
from environ import ImproperlyConfigured
from sentry_sdk.integrations.django import DjangoIntegration

from .base import *

DEBUG = False
WAGTAIL_ENABLE_UPDATE_CHECK = False

RECAPTCHA_PUBLIC_KEY = env("GOOGLE_RECAPTCHA_SITEKEY")
RECAPTCHA_PRIVATE_KEY = env("GOOGLE_RECAPTCHA_SECRET")

# ManifestStaticFilesStorage is recommended in production, to prevent outdated
# JavaScript / CSS assets being served from cache (e.g. after a Wagtail upgrade).
# See https://docs.djangoproject.com/en/4.1/ref/contrib/staticfiles/#manifeststaticfilesstorage
# STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"
# TODO enable when all `static` are available.
# STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

try:
    SECRET_KEY = env("DJANGO_SECRET_KEY")
except ImproperlyConfigured:
    SECRET_KEY = "".join(
        [
            random.SystemRandom().choice(
                "{}{}{}".format(string.ascii_letters, string.digits, "+-:$;<=>?@^_~")
            )
            for i in range(63)
        ]
    )
    with open(ENV_PATH, "a") as envfile:
        envfile.write(f"DJANGO_SECRET_KEY={SECRET_KEY}\n")

sentry_sdk.init(
    dsn=env("DJANGO_SENTRY_DSN"),
    integrations=[DjangoIntegration()],
    send_default_pii=True,
)


# INSTALLED_APPS += ("djangosecure",)

# LOGGING = {
#     "version": 1,
#     "disable_existing_loggers": True,
#     "root": {"level": "WARNING", "handlers": ["sentry"]},
#     "formatters": {
#         "verbose": {
#             "format": "%(levelname)s %(asctime)s %(module)s "
#             "%(process)d %(thread)d %(message)s"
#         },
#     },
#     "handlers": {
#         "console": {
#             "level": "DEBUG",
#             "class": "logging.StreamHandler",
#             "formatter": "verbose",
#         },
#     },
#     "loggers": {
#         "django.db.backends": {
#             "level": "ERROR",
#             "handlers": ["console"],
#             "propagate": False,
#         },
#         "sentry.errors": {
#             "level": "DEBUG",
#             "handlers": ["console"],
#             "propagate": False,
#         },
#         "django.security.DisallowedHost": {
#             "level": "ERROR",
#             "handlers": ["console", "sentry"],
#             "propagate": False,
#         },
#     },
# }
