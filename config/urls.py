from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path, include
from django.views import defaults as default_views
from django.views.generic import RedirectView, TemplateView
from wagtail.contrib.sitemaps.views import sitemap
from wagtail.documents import urls as wagtaildocs_urls

from apps.cms.views import AddLanguagePrefixRedirectView
from apps.core.views import SwitchLanguageView
from apps.medialibrary.views import MediaDetailView
from apps.search.views import SearchView
from apps.users.views import ReactivateAccountView

BACKEND_NAME = "WOCAT backend"
admin.site.site_header = BACKEND_NAME
admin.site.site_title = BACKEND_NAME

urlpatterns = [
    path("sitemap.xml", sitemap),
    # Django Admin, use {% url 'admin:index' %}
    path(f"{settings.ADMIN_URL}/", admin.site.urls),
    path("users/", include(("apps.users.urls", "users"), namespace="users")),
    path(
        "accounts/reactivate/",
        ReactivateAccountView.as_view(),
        name="reactivate_account",
    ),
    path("accounts/", include("allauth.urls")),
    path("library/media/<int:pk>/", MediaDetailView.as_view(), name="media_detail"),
    # Redirect requests to fileadmin/* (old document folder) to new media library
    path("fileadmin/", RedirectView.as_view(url="/en/wocat-media-library")),
    path("glossary/", include("apps.glossary.urls")),
    path("institutions/", include("apps.institutions.urls")),
    path("countries/", include("apps.countries.urls")),
    path("search/", SearchView.as_view(), name="search_index"),
    path("cmsviews/", include("apps.cms.urls")),
    path("newsletter/", include("apps.newsletter.urls")),
    # Your stuff: custom urls includes go here
    path("i18n/", SwitchLanguageView.as_view(), name="set_language"),
    path("api/", include("apps.api.urls")),
    # Users should not be deleted! Manually catching requests to delete users
    # in the CMS and showing them a message.
    path(
        "cms/users/<int:pk>/delete/",
        view=TemplateView.as_view(template_name="users/confirm_delete.html"),
        name="cms_user_delete",
    ),
    # CMS
    path("cms/", include("wagtail.admin.urls")),
    path("documents/", include(wagtaildocs_urls)),
]
# Static files
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
        path("styleguide/", include("apps.styleguide.urls")),
    ]

urlpatterns += [
    re_path(
        r"^(?!({})|static|media)(.+)".format(
            "|".join([l[0] for l in settings.LANGUAGES])
        ),
        AddLanguagePrefixRedirectView.as_view(),
    ),
    path("", include("wagtail.urls")),
]
